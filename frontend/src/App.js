import React, { Component } from "react";
import { Layout, Spin } from "antd";
//import Filter from './components/Filter/Filter'
import Structure from "./components/Structure/Structure";
//import Evaluation from './components/Evaluation/Evaluation';
import "./App.css";
import Hero from "./components/Hero/Hero";
import { createFilter } from "./util/Filter";

const { Content, Footer } = Layout;

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: null,
      city: "",
      type: "",
      year: "",
      multiple: false,
      filteredData: null,
      filters: this.props.filters
    };
  }

  static defaultProps = {
    filters: [
      {
        property: "city",
        value: "Recife"
      }
    ]
  };

  componentDidMount() {
    fetch("https://ideciclo-backend.herokuapp.com/")
      .then(response => response.json())
      .then(data => this.setState({ data: data, filteredData: data }))
  }

  onFilter = (val, type) => {
    switch (type) {
      case "city":
        this.setState({ city: val });
        this.setState({
          filteredData: this.state.data.filter(
            structure => structure.city === val
          )
        });
        break;
      case "year":
        this.setState({ year: val });
        break;
      case "type":
        this.setState({ type: val });
        this.setState({
          filteredData: this.state.data.filter(
            structure => structure.structureType.name === val
          )
        });
        break;
      default:
        this.setState({
          filteredData: this.state.data
        });
        break;
    }
  };

  render() {
    if (!this.state.data) {
      return (
        <div className="App">
          <Spin size="large" className="spinner--loading" />
        </div>
      );
    }

    const cityOptions = this.state.data
      .map(option => {
        return option.city;
      })
      .reduce((x, y) => (x.includes(y) ? x : [...x, y]), []);

    const typeOptions = this.state.data
      .map(option => {
        return option.structureType.name;
      })
      .reduce((x, y) => (x.includes(y) ? x : [...x, y]), []);

    return (
      <div className="App">
        <Hero
          onFilter={this.onFilter}
          cityOptions={cityOptions}
          typeOptions={typeOptions}
        />
        <Layout className="layout">
          <Content>
            {/* <div>
              <Filter onFilter={this.onFilter} type='city' options={cityOptions} placeholder='Selecione uma cidade' />
              <Filter onFilter={this.onFilter} type='type' options={typeOptions} placeholder='Selecione um tipo de estrutura' />
            </div> */}
            {/* <Evaluation data={this.state.data} /> */}
            <Structure data={this.state.filteredData} />
          </Content>
          <Footer style={{ textAlign: "center" }} />
        </Layout>
      </div>
    );
  }
}

export default App;
