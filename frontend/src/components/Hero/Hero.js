import React, { Component } from 'react'
import Filter from './../Filter/Filter'
import './Hero.css';

export default class Hero extends Component {

  onFilter = this.props.onFilter

  
  render() {
    return (
      <div className="hero">
        <div className="hero-content">
          <div className="hero-text">
            <h1>IDECICLO</h1>
            <p>O Índice de Desenvolvimento Cicloviário (IDECICLO) tem como objetivo avaliar qualitativamente a 
              infraestrutura cicloviária da cidade de forma objetiva e replicável de modo que haja acompanhamento 
              da evolução dos parâmetros para uma comparação entre infraestruturas e entre cidades, de maneira a construir uma séria histórica.</p>
          </div>
          <div className="hero-filter__container">
            <Filter onFilter={this.onFilter} type='city' options={this.props.cityOptions} placeholder='Selecione uma cidade' />
            <Filter onFilter={this.onFilter} type='type' options={this.props.typeOptions} placeholder='Selecione um tipo de estrutura' />
          </div>
        </div>
      </div>
    )
  }
}
