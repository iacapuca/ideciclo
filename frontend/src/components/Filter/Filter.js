import React, { Component } from 'react'
import { Select } from 'antd';

const Option = Select.Option;



export default class Filter extends Component {
    constructor(props) {
        super(props);

        this.state = {
        }
    }

    //  handleChange = (city) => {
    //     this.props.onCityFilter(city);
    //     this.setState({ cityFilter: city });
    // }

    handleChange = (val) => {
        if(val){
            this.props.onFilter(val, this.props.type);
        } else if(!val) {
            this.props.onFilter(val, null);
        }
    }


    
    render() {
        const optionList = this.props.options.map(option => {
            return (
                <Option key={option} value={option}>{option}</Option>
            )
        })
        return (
            <div className="filters-container">
                <Select
                    showSearch={true}
                    allowClear={true}
                    placeholder={this.props.placeholder}
                    style={{ width: 250 }}
                    onChange={this.handleChange}
                    type={this.props.type}
                >
                    {optionList}
                </Select>
            </div>
        )
    }
}
