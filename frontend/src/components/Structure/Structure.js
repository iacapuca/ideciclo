import React, { Component } from 'react'
import { Table, Modal, Input, Button, Icon } from 'antd';
import './Structure.css'


export default class extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      record: {'structureType':{}, 'reviews':[{}]},
      searchText: '',
      data: this.props.data,
    }
  };

  showModal = (record) => {

    fetch('http://api.ideciclo.ameciclo.org/structures/' + record.id)
    .then(response => response.json())
    .then(data => this.setState({"record":data}));
    console.log(record);
    this.setState({
      visible: true,
    });
  }

  handleOk = (e) => {
    this.setState({
      visible: false,
      record: {'structureType':{}, 'reviews':[{}]},
    });

  }

  handleCancel = (e) => {
    this.setState({
      visible: false,
      record: {'structureType':{}, 'reviews':[{}]},
    });
  }

  handleSearch = (selectedKeys, confirm) => () => {
    confirm();
    this.setState({ searchText: selectedKeys[0] });
  }

  handleReset = clearFilters => () => {
    clearFilters();
    this.setState({ searchText: '' });
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) {
      this.setState({ data: this.props.data })
    }
  }

  render() {


    const columns = [{
      title: 'Tipo',
      dataIndex: 'structureType.name',
      key: 'structureType.name',
      filters: [{
        text: 'Ciclofaixa',
        value: 'Ciclofaixa',
      }, {
        text: 'Ciclovia',
        value: 'Ciclovia',
      },
      {
        text: 'Ciclorrota',
        value: 'Ciclorrota',
      },
      {
        text: 'Calçada Compartilhada',
        value: 'Calçada Compartilhada',
      },
    ],
      onFilter: (value, record) => record.structureType.name.indexOf(value) === 0,
    }, {
      title: 'Rua',
      dataIndex: 'street',
      key: 'street',
      filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
        <div className="custom-filter-dropdown">
          <Input
            ref={ele => this.searchInput = ele}
            placeholder="Procurar por rua"
            value={selectedKeys[0]}
            onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
            onPressEnter={this.handleSearch(selectedKeys, confirm)}
          />
          <Button type="primary" onClick={this.handleSearch(selectedKeys, confirm)}>Procurar</Button>
          <Button onClick={this.handleReset(clearFilters)}>Limpar</Button>
        </div>
      ),
      filterIcon: filtered => <Icon type="filter" style={{ color: filtered ? '#108ee9' : '#aaa' }} />,
      onFilter: (value, record) => record.street.toLowerCase().includes(value.toLowerCase()),
      onFilterDropdownVisibleChange: (visible) => {
        if (visible) {
          setTimeout(() => {
            this.searchInput.focus();
          });
        }
      },
      render: (text) => {
        const { searchText } = this.state;
        return searchText ? (
          <span>
            {text.split(new RegExp(`(?<=${searchText})|(?=${searchText})`, 'i')).map((fragment, i) => (
              fragment.toLowerCase() === searchText.toLowerCase()
                ? <span key={i} className="highlight">{fragment}</span> : fragment // eslint-disable-line
            ))}
          </span>
        ) : text;
      },
    }, {
      title: 'Extensão',
      dataIndex: 'extension',
      key: 'extension',
      sorter: (a, b) => a.extension - b.extension,
    }, {
      title: 'Média',
      dataIndex: 'average_rating',
      key: 'average_rating',
      sorter: (a, b) => a.average_rating - b.average_rating,
    }];
    return (
      <div>
        <Table dataSource={this.state.data} 
        columns={columns} 
        rowKey='id'
        pagination={false} 
        onRow={(record) => ({
          onClick: () => {
            this.showModal(record);
          },
        })} />
        <Modal
          title={this.state.record.street}
          visible={this.state.visible}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
          footer={null}
        >
        <div className="modal-data-container">
          <h1>DADOS GERAIS:</h1>
          <p>Tipologia:{this.state.record.structureType.name + ' ' + this.state.record.structureType.typology}</p>
          <p>Extensão da estrutura:{this.state.record.extension}</p>
          <p>Velocidade máxima da via:{this.state.record.max_speed}</p>
          <p>Cidade:{this.state.record.city}</p>
          <p>Data de Avaliação:{this.state.record.reviews[0].reviewed_at}</p>
        </div>
        <div className="modal-data-container">
          <h1>NOTAS:</h1>
          <div className="badge">{this.state.record.reviews[0].average_rating}</div>
          <p>Segurança:{this.state.record.reviews[0].safety_rating}</p>
          <p>Conforto:{this.state.record.reviews[0].comfort_rating}</p>
        </div>
        </Modal>
      </div>
    )
  }
}
