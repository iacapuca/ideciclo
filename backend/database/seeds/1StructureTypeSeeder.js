'use strict'

/*
|--------------------------------------------------------------------------
| StructureTypeSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Database = use('Database')
const uuidv4 = require('uuid/v4');

class StructureTypeSeeder {
  async run() {
    await Database.table('structure_types').del()
    await Database.table('structure_types').insert([{
        structureTypeId: uuidv4(),
        name: 'Ciclofaixa',
        typology: 'Unidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Ciclofaixa',
        typology: 'Bidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Ciclovia',
        typology: 'Unidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Ciclovia',
        typology: 'Bidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Ciclorrota',
        typology: 'Unidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Ciclorrota',
        typology: 'Bidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Calçada Compartilhada',
        typology: 'Unidirecional'
      },
      {
        structureTypeId: uuidv4(),
        name: 'Calçada Compartilhada',
        typology: 'Bidirecional'
      },
    ])
  }
}

module.exports = StructureTypeSeeder
