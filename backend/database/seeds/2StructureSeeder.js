'use strict'

/*
|--------------------------------------------------------------------------
| StructureSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Database = use('Database')
const uuidv4 = require('uuid/v4');
const structureData = require('./structure.json');

class StructureSeeder {
  async run() {
    await Database.table('structures').del()
      structureData.map(async (structure) => {
         await Database.table('structures').insert([{
          id: uuidv4(),
          street: structure.street,
          city: structure.city,
          max_speed: structure.max_speed,
          extension: structure.extension,
          structure_type_id: Database.table('structure_types').select('structureTypeId').where({
            'name': structure.type,
            'typology': structure.typology,
          }),
          created_at: new Date(),
          updated_at: new Date(),
        }]);
      });
  }
}

module.exports = StructureSeeder
