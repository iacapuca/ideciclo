'use strict'

/*
|--------------------------------------------------------------------------
| ReviewerSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Database = use('Database')
const uuidv4 = require('uuid/v4');

class ReviewerSeeder {
  async run() {
    await Database.table('reviewers').del()
    await Database.table('reviewers').insert([
      {
        reviewerId: uuidv4(),
        name: 'Gaia Lourenço'
      },
      {
        reviewerId: uuidv4(),
        name: 'Diego Dutra'
      },
      {
        reviewerId: uuidv4(),
        name: 'Josevaldo Bezerra'
      },
      {
        reviewerId: uuidv4(),
        name: 'Carolina Farias'
      },
      {
        reviewerId: uuidv4(),
        name: 'Janaína Barros'
      },
      {
        reviewerId: uuidv4(),
        name: 'Jackson Roberto'
      },
      {
        reviewerId: uuidv4(),
        name: 'Daniel Valença'
      },
    ])
  }
}

module.exports = ReviewerSeeder
