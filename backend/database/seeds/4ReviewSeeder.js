'use strict'

/*
|--------------------------------------------------------------------------
| ReviewSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Database = use('Database')
const uuidv4 = require('uuid/v4');
const reviewData = require('./review.json');


class ReviewSeeder {
  async run() {
    await Database.table('reviews').del()
    reviewData.map(async (review) => {
      await Database.table('reviews').insert([{
        id: uuidv4(),
        structure_id:  Database.table('structures').select('id').where({ 'street': review.street }), 
        reviewed_at: review.reviewed_at,
        reviewer_id:  Database.table('reviewers').select('reviewerId').where({ 'name': review.reviewer }),
        average_rating: review.average_rating,
        adequacy_rating: review.adequacy_rating,
        protection_rating: review.protection_rating,
        speed_control_rating: review.speed_control_rating,
        hor_cross_sign_rating: review.hor_cross_sign_rating,
        avg_structure_width: review.avg_structure_width,
        hor_sign_rating: review.hor_sign_rating,
        ver_sign_rating: review.ver_sign_rating,
        ver_cross_sign_rating: review.ver_cross_sign_rating,
        pattern_paint_rating: review.pattern_paint_rating,
        hor_sign_condition_rating: review.hor_sign_condition_rating,
        risk_rating: review.risk_rating,
        sinuosity_rating: review.sinuosity_rating,
        bidirectionality_rating: review.bidirectionality_rating,
        shading_rating: review.shading_rating,
        type_pavement_rating: review.type_pavement_rating,
        pavement_condition_rating: review.pavement_condition_rating,
        obstacles_rating: review.obstacles_rating, 
        safety_rating: review.safety_rating,
        comfort_rating: review.comfort_rating,
        created_at: new Date(),
        updated_at: new Date(),
      }]);
    });
  }
}

module.exports = ReviewSeeder
