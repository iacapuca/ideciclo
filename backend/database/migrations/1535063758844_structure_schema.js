'use strict'

const Schema = use('Schema')

class StructureSchema extends Schema {
  up () {
    this.dropIfExists('structures')
    this.create('structures', (table) => {
      table.uuid('id').primary();
      table.string('street')
      table.string('city')
      table.integer('max_speed')
      table.float('extension')
      table.uuid('structure_type_id').unsigned()
      table.foreign('structure_type_id').references('structure_types.structureTypeId')
      table.timestamps()
    })
  }

  down () {
    this.drop('structures')
  }
}

module.exports = StructureSchema
