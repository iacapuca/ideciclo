'use strict'

const Schema = use('Schema')

class ReviewSchema extends Schema {
  up () {
    this.dropIfExists('reviews')
    this.create('reviews', (table) => {
      table.uuid('id').primary()
      table.uuid('structure_id').unsigned()
      table.foreign('structure_id').references('structures.id')
      table.uuid('reviewer_id').unsigned()
      table.foreign('reviewer_id').references('reviewers.reviewerId')
      table.float('average_rating')
      table.float('safety_rating')
      table.float('comfort_rating')
      table.float('adequacy_rating')
      table.float('protection_rating')
      table.float('speed_control_rating')
      table.float('hor_cross_sign_rating')
      table.float('avg_structure_width')
      table.float('hor_sign_rating')
      table.float('ver_sign_rating')
      table.float('ver_cross_sign_rating')
      table.float('pattern_paint_rating')
      table.float('hor_sign_condition_rating')
      table.float('risk_rating')
      table.float('sinuosity_rating')
      table.float('bidirectionality_rating')
      table.float('shading_rating')
      table.float('type_pavement_rating')
      table.float('pavement_condition_rating')
      table.float('obstacles_rating')
      table.date('reviewed_at')
      table.timestamps()
    })
  }

  down () {
    this.drop('reviews')
  }
}

module.exports = ReviewSchema
