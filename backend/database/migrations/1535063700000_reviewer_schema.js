'use strict'

const Schema = use('Schema')

class ReviewerSchema extends Schema {
  up () {
    this.dropIfExists('reviewers')
    this.create('reviewers', (table) => {
      table.uuid('reviewerId').primary();
      table.string('name')
    })
  }

  down () {
    this.drop('reviewers')
  }
}

module.exports = ReviewerSchema
