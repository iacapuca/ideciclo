'use strict'

const Schema = use('Schema')

class StructureTypeSchema extends Schema {
  up () {
    this.dropIfExists('structure_types')
    this.create('structure_types', (table) => {
      table.uuid('structureTypeId').primary();
      table.string('name')
      table.string('typology')

    })
  }

  down () {
    this.drop('structure_types')
  }
}

module.exports = StructureTypeSchema
