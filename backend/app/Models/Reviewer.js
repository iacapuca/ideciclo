'use strict'

const Model = use('Model')

class Reviewer extends Model {

    review () {
        return this.belongsTo('App/Models/Review')
      }

}

module.exports = Reviewer
