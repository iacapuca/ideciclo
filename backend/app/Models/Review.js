'use strict'

const Model = use('Model')

class Review extends Model {

  static get hidden () {
    return ['reviewer_id']
  }


    structure () {
        return this.belongsTo('App/Models/Structure')
      }

      reviewer () {
        return this.hasOne('App/Models/Structure')
      }

}

module.exports = Review
