'use strict'

const Model = use('Model')

class Structure extends Model {

  static get hidden () {
    return ['structure_type_id']
  }

  static get computed () {
    return ['average_rating']
  }

  getAverageRating () {
    let average_rating = this.getRelated('reviews').rows.map((review) => {
      return review.average_rating
    })
    return average_rating[0]
  }

  reviews() {
    return this.hasMany('App/Models/Review')
  }

  structureType() {
    return this.hasOne('App/Models/StructureType', 'structure_type_id', 'structureTypeId')
  }
}

module.exports = Structure
