const TransformerAbstract = use('Adonis/Addons/Bumblebee/TransformerAbstract')

class ReviewTransformer extends TransformerAbstract {
  transform (model) {
    return {
      id: model.id,
      structure_id: model.structure_id,
      average_rating: model.average_rating,
      safety_rating: model.safety_rating,
      comfort_rating: model.comfort_rating,
      adequacy_rating: model.adequacy_rating,
      protection_rating: model.protection_rating,
      speed_control_rating: model.speed_control_rating,
      hor_cross_sign_rating: model.hor_cross_sign_rating,
      avg_structure_width: model.avg_structure_width,
      hor_sign_rating: model.hor_sign_rating,
      ver_sign_rating: model.ver_sign_rating,
      ver_cross_sign_rating: model.ver_cross_sign_rating,
      pattern_paint_rating: model.pattern_paint_rating,
      hor_sign_condition_rating: model.hor_sign_condition_rating,
      risk_rating: model.risk_rating,
      sinuosity_rating: model.sinuosity_rating,
      bidirectionality_rating: model.bidirectionality_rating,
      shading_rating: model.shading_rating,
      type_pavement_rating: model.type_pavement_rating,
      pavement_condition_rating: model.pavement_condition_rating,
      obstacles_rating: model.obstacles_rating,
      reviewed_at: model.reviewed_at
    }
  }
}

module.exports = ReviewTransformer