const TransformerAbstract = use('Adonis/Addons/Bumblebee/TransformerAbstract')
const StructureTypeTransformer = use('./StructureTypeTransformer.js')
const ReviewTransformer = use('./ReviewTransformer.js')


class StructureTransformer extends TransformerAbstract {

    defaultInclude () {
        return [
          'structureType',
          'reviews'
        ]
      }

  transform (model) {
    return {
      id: model.id,

      street: model.street,
      city: model.city,
      max_speed: model.max_speed,
      extension: model.extension,
      created_at: model.created_at,
      updated_at: model.updated_at,
      average_rating: model.average_rating,

    }
  }

  includeStructureType (model) {
    return this.item(model.getRelated('structureType'), StructureTypeTransformer)
  }
  includeReviews (model) {
    return this.collection(model.getRelated('reviews'), ReviewTransformer)
  }
}

module.exports = StructureTransformer