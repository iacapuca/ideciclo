const TransformerAbstract = use('Adonis/Addons/Bumblebee/TransformerAbstract')

class StructureTypeTransformer extends TransformerAbstract {
  transform (model) {
    return {
      id: model.id,

      name: model.name,
      typology: model.typology,
    }
  }
}

module.exports = StructureTypeTransformer