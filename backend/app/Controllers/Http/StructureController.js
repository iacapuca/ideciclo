'use strict'

const Structure = use('App/Models/Structure')


/**
 * Resourceful controller for interacting with structures
 */
class StructureController {
  /**
   * Show a list of all structures.
   * GET structures
   */
  async index ({}) {
    const structures = await Structure
      .query()
      .with('structureType')
      .with('reviews', (builder) => {
        builder
        .select(['structure_id', 'average_rating', 'reviewed_at'])
        .orderBy('reviewed_at', 'desc')
      })      
      .fetch()
    return structures

  }

  /**
   * Create/save a new structure.
   * POST structures
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single structure.
   * GET structures/:id
   */
  async show ({ params:{id}, request, response, view }) {
    const structure = await Structure
    .query()
    .with('structureType')
    .where('id', id)
    .with('reviews', (builder) => {
      builder
      .select(['structure_id', '*'])
      .orderBy('reviewed_at', 'desc')
    })      
    .first()
    return structure
  }

  /**
   * Update structure details.
   * PUT or PATCH structures/:id
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a structure with id.
   * DELETE structures/:id
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = StructureController
