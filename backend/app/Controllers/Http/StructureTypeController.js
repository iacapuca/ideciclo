'use strict'

/**
 * Resourceful controller for interacting with structuretypes
 */
class StructureTypeController {
  /**
   * Show a list of all structuretypes.
   * GET structuretypes
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for creating a new structuretype.
   * GET structuretypes/create
   */
  async create ({ request, response, view }) {
  }

  /**
   * Create/save a new structuretype.
   * POST structuretypes
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single structuretype.
   * GET structuretypes/:id
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing structuretype.
   * GET structuretypes/:id/edit
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update structuretype details.
   * PUT or PATCH structuretypes/:id
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a structuretype with id.
   * DELETE structuretypes/:id
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = StructureTypeController
