'use strict'

/**
 * Resourceful controller for interacting with reviewers
 */
class ReviewerController {
  /**
   * Show a list of all reviewers.
   * GET reviewers
   */
  async index ({ request, response, view }) {
  }

  /**
   * Create/save a new reviewer.
   * POST reviewers
   */
  async store ({ request, response }) {
  }

  /**
   * Display a single reviewer.
   * GET reviewers/:id
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Update reviewer details.
   * PUT or PATCH reviewers/:id
   */
  async update ({ params, request, response }) {
  }

  /**
   * Delete a reviewer with id.
   * DELETE reviewers/:id
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = ReviewerController
